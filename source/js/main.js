$(".swiper-container").each(function(index, element){
    var $this = $(this);
    new Swiper(this, {
    slidesPerView: 'auto',
    spaceBetween: 10,
        navigation: {
            nextEl:  $this.parent().find(".swiper-button-next")[0],
            prevEl: $this.parent().find(".swiper-button-prev")[0],
        },
        
    });
});
