'use strict';

/* ========================================= *
 *           D E F I N I T I O N S           *
 * ========================================= */

// Plugins
const   gulp = require('gulp'),
        notify = require("gulp-notify"),
        sass = require('gulp-sass'),
        plumber = require('gulp-plumber'),
        sourcemaps = require('gulp-sourcemaps'),
        concat = require('gulp-concat'),
        autoprefixer = require('gulp-autoprefixer'),
        stripCssComments = require('gulp-strip-css-comments'),
        pug = require('gulp-pug'),
        minify = require('gulp-minify'),
        imagemin = require('gulp-imagemin'),
        browserSync = require('browser-sync').create(),
        imageminMozjpeg = require('imagemin-mozjpeg'),
        imageminPngquant = require('imagemin-pngquant'),
        del = require('del');

// Directory
const   directory ={
    source: 'source',
    dist: 'public'
};

const   path = {
    css: {
        sass: directory.source + '/scss/*.scss',
        source: directory.source + '/css/',
        all: [directory.source + '/css/**/*.css'],
        dist: directory.dist + '/css/'
    },
    js: {
        source: directory.source + '/js/',
        all: [directory.source + '/js/**/*.js'],
        dist: directory.dist + '/js/'
    },
    html: {
        pug: directory.source + '/*.pug',
        html: directory.dist
    },
    images: {
        source: directory.source + '/images/**/*.{gif,jpg,png,svg}', 
        dist: directory.dist + '/images/',
    },
    favicon: {
        source: directory.source + '/icon/**/*',
        dist: directory.dist + '/images/icon/',
    },
    fonts: {
        source: directory.source + '/fonts/**/*.{eot,svg,ttf,woff,woff2}',
        dist: directory.dist + '/fonts/',
    }
};

// Javascript Plugins & Libraries
const   components = {
    css: [
        directory.source + '/vendor/css/**/*.css',
    ],
    js:[
        directory.source + '/vendor/js/**/*.js'
    ]
};

/* ========================================= *
 *             F U N C T I O N S             *
 * ========================================= */

var onError = function(err) {
    notify.onError({
      title:    "Gulp",
      subtitle: "Failure!",
      message:  "Error: <%= error.message %>",
      sound:    "Basso"
    })(err);
    this.emit('end');
  };

/* ========================================= *
 *                 T A S K S                 *
 * ========================================= */

//Includes all the css libraries of the vendor directory
gulp.task('lib:css', function() {
    return gulp.src(components.css)
        .pipe(concat('components.css'))
        .pipe(gulp.dest(path.css.source));
});

//Includes all the js libraries of the vendor directory
gulp.task('lib:js', function() {
    return gulp.src(components.js)
        .pipe(concat('components.js'))
        .pipe(gulp.dest(path.js.source));
});

/* ========================================= *
 *         P R E P R O C E S S I N G
 * ========================================= */

gulp.task('sass:dev', function(){
    return gulp.src(path.css.sass)
        .pipe(plumber({errorHandler: onError}))
        .pipe(sass({outputStyle: 'expanded'}))
        .pipe(autoprefixer({
            browsers: ['last 3 versions'],
            cascade: false
        }))
        .pipe(concat('sass.css'))
        .pipe(gulp.dest(path.css.source));
});

gulp.task('sass:prod', function(){
    return gulp.src(path.css.sass)
        .pipe(plumber({errorHandler: onError}))
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(autoprefixer({
            browsers: ['last 3 versions'],
            cascade: false
        }))
        .pipe(concat('sass.css'))
        .pipe(stripCssComments({preserve:false}))
        .pipe(gulp.dest(path.css.source));
});

gulp.task('pug:dev', function () {
    return gulp.src(path.html.pug)
        .pipe(plumber({errorHandler: onError}))
        .pipe(pug({pretty: true}))
        .pipe(gulp.dest(path.html.html))
        .pipe(browserSync.reload({
            stream: true
        }));
});

gulp.task('pug:prod', function () {
    return gulp.src(path.html.pug)
        .pipe(plumber({errorHandler: onError}))
        .pipe(pug({pretty: false}))
        .pipe(gulp.dest(path.html.html));
});

/* ========================================= *
 *          O P T I M I Z A T I O N
 * ========================================= */

gulp.task('styles:dev', function(){
    return gulp.src(path.css.all)
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(concat('main.min.css'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(path.css.dist))
        .pipe(browserSync.reload({
            stream: true
        }));
});

gulp.task('styles:prod', function(){
    return gulp.src(path.css.all)
        .pipe(plumber())
        .pipe(concat('main.min.css'))
        .pipe(minify())
        .pipe(gulp.dest(path.css.dist))
        .pipe(browserSync.reload({
            stream: true
        }));
});

gulp.task('javaScript:dev',function(){
    return gulp.src(path.js.all)
        .pipe(plumber({errorHandler: onError}))
        .pipe(concat('main.min.js'))
        .pipe(gulp.dest(path.js.dist))
        .pipe(browserSync.stream());
});

gulp.task('javaScript:prod',function(){
    return gulp.src(path.js.all)
        .pipe(plumber({errorHandler: onError}))
        .pipe(minify({comments:true}))
        .pipe(concat('main.min.js'))
        .pipe(gulp.dest(path.js.dist))
        .pipe(browserSync.stream());
});

gulp.task('images', function(){
    return gulp.src(path.images.source)
        .pipe(imagemin([
            imageminMozjpeg({quality: "80"}),
            imageminPngquant({quality: [0.5,0.8], strip: true}),
            imagemin.gifsicle({interlaced: true}),
            imagemin.svgo({
                plugins: [
                    {removeViewBox: true},
                    {cleanupIDs: false}
                ]
            })
        ]))
        .pipe(gulp.dest(path.images.dist))
        .pipe(browserSync.reload({
            stream: true
        }));
});

gulp.task('favicon', function () {
    return gulp.src(path.favicon.source)
        .pipe(gulp.dest(path.favicon.dist))
        .pipe(browserSync.reload({
            stream: true
        }));
});

gulp.task('fonts', function(){
    return gulp.src(path.fonts.source)
        .pipe(gulp.dest(directory.dist + "/fonts"))
        .pipe(browserSync.stream());
});

/* ========================================= *
 *                  C L E A N          
 * ========================================= */

gulp.task('clean', function () {
    return del([
        directory.dist + '/**/*'
    ], {force: true});
});

gulp.task('copy:include', function () {
    return gulp.src(directory.source + '/configurations/**/*.*').
    pipe(gulp.dest(directory.dist));
});


/* ========================================= *
 *           D E V E L O P M E N T
 * ========================================= */

gulp.task('watch',function () {
    browserSync.init({
        server: directory.dist
    });
    gulp.watch(directory.source + '/configurations/**/*.*', gulp.series('copy:include'));
    gulp.watch(components.css, gulp.series('lib:css'));
    gulp.watch(components.js, gulp.series('lib:js'));
    gulp.watch(path.css.sass, gulp.series('sass:dev'));
    gulp.watch(path.html.pug, gulp.series('pug:dev'));

    gulp.watch(path.js.all, gulp.series('javaScript:dev'));
    gulp.watch(path.images.source, gulp.series('images'));
    gulp.watch(path.favicon.source, gulp.series('favicon'));
    gulp.watch(path.fonts.source, gulp.series('fonts'));
    gulp.watch(path.html.pug).on('change', browserSync.reload);
});





/* ========================================= *
 *                  T A S K
 * ========================================= */

gulp.task('default', gulp.series('clean','lib:css', 'lib:js', 'sass:dev', 'pug:dev', 'styles:dev', 'javaScript:dev', 'images', 'favicon', 'fonts', 'watch'));
gulp.task('production', gulp.series('clean','copy:include', 'lib:css', 'lib:js', 'sass:prod', 'pug:prod', 'styles:dev', 'javaScript:prod', 'images', 'favicon', 'fonts'));